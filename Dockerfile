FROM python:slim-buster

RUN apt update \
    && apt upgrade \
    && apt install wget -y \
    && pip3 install numpy \
    && wget https://wix-software-updates.s3.amazonaws.com/braille/braille-linux-amd64 && chmod +x braille-linux-amd64
