import numpy as np

try:
    input = str(input())
except:
    exit(1)

# Main function use room number to print to stdout
def _china_3d_printer(room):
    brailleNumbers = {
        '1': [['*','^'],['^','^'],['^','^']],
        '2': [['*','^'],['*','^'],['^','^']],
        '3': [['*','*'],['^','^'],['^','^']],
        '4': [['*','*'],['^','*'],['^','^']],
        '5': [['*','^'],['^','*'],['^','^']],
        '6': [['*','*'],['*','^'],['^','^']],
        '7': [['*','*'],['*','*'],['^','^']],
        '8': [['*','^'],['*','*'],['^','^']],
        '9': [['^','*'],['*','^'],['^','^']],
        '0': [['^','*'],['*','*'],['^','^']],
        }
    room = list(input)
    firstDigit = brailleNumbers[room[0]]
    secondDigit = brailleNumbers[room[1]]
    thirdDigit = brailleNumbers[room[2]]
    # Build matrix forall 3 digits joined
    matrix = np.hstack(( firstDigit,secondDigit,thirdDigit ))
    # Print 1st line from left to right
    for i in  matrix[0]:
        print(i, end = '')
    # Print 2nd line from right to left
    for i in  reversed(matrix[1]):
        print(i, end = '')
    # Print 3rd line from left to right
    for i in matrix[2]:
        print(i, end = '')

# Validate user input and call function
if len(input) == 3 and input.isdecimal():
    _china_3d_printer(input)
else:
    exit(1)
